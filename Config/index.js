'use strict'

const mongoose = require('mongoose');
const app = require('../Server/index');
const port = 3999;

//generar promesa
mongoose.Promise = global.Promise;

//hacer conexion a mongo

mongoose.connect('mongodb://localhost:27017/tienda', { useNewUrlParser:true })
    .then (() => {
        console.log('base de datos corriendo en local');

        //escuchar el servidor
        app.listen(port, () =>{
            console.log(`server corriendo en el puerto: ${port}`);
        })
    })