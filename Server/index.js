//llamar las librerias
'use strict'
const bodyParser = require('body-parser');
const express = require('express');

//iniciamos express

const app = express();

//iniciamos cors
app.use(function(req, rest, next){
    rest.header("access-Control-Allow-Origin", "*");
    rest.header("access-Control-Allow-Headers", 
    "Origin, X-Requested-Width, Content-Type, Accept");
    rest.header("access-Control-Allow-Methods", 
    "GET, POST, OPTIONS, PUT, PATCH, DELETE");

    next();
});

//Middlewares

app.use(bodyParser.urlencoded({extended: false})); //decirle que no aceptara cualquier tipo de documento,  para que lo transforme en json
app.use(bodyParser.json()); //aca dice que los elementos seran en formato json

app.use(require('../Routes/index')); //pasa la ruta de la lista de los controladores 

module.exports = app;